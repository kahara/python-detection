"""Test the detector base class"""

from pytest import approx  # type: ignore
import numpy as np  # type: ignore

from scenegen.testscene import TestScene

from detection.detector import ConfigType
from detection.detectors.blob.detector import BlobDetector


def test_detector_blob() -> None:
    """Test the blob detector"""

    width: int = 1920
    height: int = 1080
    length: int = 320

    config: ConfigType = {"blur": 7, "accelerate": True}
    instance = BlobDetector(config)
    scene = TestScene(scene="balls", width=width, height=height, length=length)

    for frame, boundingboxes in scene.frames():
        (_boundingboxes, _metadata) = instance.feed(
            np.reshape(np.frombuffer(frame, dtype=np.uint8), newshape=(height, width, 3))
        )
        if not boundingboxes:
            continue

        assert len(_boundingboxes) == 1
        assert boundingboxes[0] == approx(_boundingboxes[iter(_boundingboxes).__next__()], abs=10)
