"""Test the detector base class"""

import numpy as np  # type: ignore

from detection.detector import ConfigType
from detection.detectors.simple.detector import SimpleDetector


def test_detector_simple() -> None:
    """Test the simple detector"""

    width: int = 1000
    height: int = 500

    config: ConfigType = {
        "threshold": 0.125,
    }

    instance = SimpleDetector(config)

    frame_light = np.zeros((height, width, 1), np.uint8)
    frame_light[:] = 128
    frame_lighter = np.zeros((height, width, 1), np.uint8)
    frame_lighter[:] = 144
    frame_lightest = np.zeros((height, width, 1), np.uint8)
    frame_lightest[:] = 160

    assert instance.feed(frame_light) == ({}, {})
    assert instance.feed(frame_light) == ({}, {})
    assert instance.feed(frame_light) == ({}, {})
    assert instance.feed(frame_lighter) == ({}, {})
    assert instance.feed(frame_light) == ({}, {})
    assert instance.feed(frame_lightest) != ({}, {})
    assert instance.feed(frame_lightest) == ({}, {})
    assert instance.feed(frame_light) != ({}, {})

    instance = SimpleDetector(config)

    frame_light = np.zeros((height, width, 3), np.uint8)
    frame_light[:] = (128, 128, 128)
    frame_lighter = np.zeros((height, width, 3), np.uint8)
    frame_lighter[:] = (144, 144, 144)
    frame_lightest = np.zeros((height, width, 3), np.uint8)
    frame_lightest[:] = (160, 160, 160)

    assert instance.feed(frame_light) == ({}, {})
    assert instance.feed(frame_light) == ({}, {})
    assert instance.feed(frame_light) == ({}, {})
    assert instance.feed(frame_lighter) == ({}, {})
    assert instance.feed(frame_light) == ({}, {})
    assert instance.feed(frame_lightest) != ({}, {})
    assert instance.feed(frame_lightest) == ({}, {})
    assert instance.feed(frame_light) != ({}, {})
