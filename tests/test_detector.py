"""Test the detector base class"""

from detection.detector import Detector


def test_detector() -> None:
    """Test the base class"""

    instance = Detector()

    assert instance.config["accelerate"]
