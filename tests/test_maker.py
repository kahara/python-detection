"""Test the maker"""
import pytest  # type: ignore

from detection.maker import make_detector
from detection.detectors.simple.detector import SimpleDetector


def test_maker() -> None:
    """Test the maker"""

    with pytest.raises(ModuleNotFoundError):
        make_detector("thisdetectordoesnotexist")

    assert isinstance(make_detector(), SimpleDetector)
    assert isinstance(make_detector("simple"), SimpleDetector)
